<?php

namespace App\Form\Admin\Posts;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class AddType extends AbstractType
{

    /**

     */
    public function __construct()
    {

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('contenu', CKEditorType::class, [
            'label' => 'Ajouter du contenu',
            'required' => true,
        ]);

        $builder->add('categorie', ChoiceType::class,[
            'choices'  => [
                'Categorie 1, partie 1' => 'C1P1',
                'Categorie 1, partie 2' => 'C1P2',
                'Categorie 1, partie 3' => 'C1P3',
                'Categorie 1, partie 4' => 'C1P4',
                'Categorie 1, partie 5' => 'C1P5',
                'Categorie 2, partie 1' => 'C2P1',
                'Categorie 2, partie 2' => 'C2P2',
                'Categorie 3, partie 1' => 'C3P1',
                'Categorie 4, partie 1' => 'C4P1',
                'Categorie 4, partie 2' => 'C4P2',
                'Categorie 4, partie 3' => 'C4P3',
                'Categorie 4, partie 4' => 'C4P4',
            ],
            'required' => true,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }
}