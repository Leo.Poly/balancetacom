<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CommunautePartie2Controller extends AbstractController
{
    #[Route('/communaute/partie2', name: 'app_communaute_partie2')]
    public function index(): Response
    {
        return $this->render('forum/partie2-commu.html.twig', [

        ]);
    }
}
