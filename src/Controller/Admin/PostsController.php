<?php

namespace App\Controller\Admin;

use App\Command\Command\Admin\Posts\AddCommand;
use App\Command\CommandHandler\Admin\Posts\AddCommandHandler;
use App\Form\Admin\Posts\AddType;
use App\Repository\PostRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/posts" , name="admin_posts")
 */
class PostsController extends AbstractController
{
    /**
     * @Route("/" , name="admin_posts_index")
     * @param PostRepository $postRepository
     * @return Response
     */
    public function index(PostRepository $postRepository): Response
    {
        return $this->render('admin/posts/index.html.twig', [
            'posts' => $postRepository->findAll()
        ]);
    }

    /**
     * @Route("/add" , name="admin_posts_add")
     * @param PostRepository $kitRepository
     * @return Response
     */
    public function add(Request $request, AddCommandHandler $handler): Response
    {
        $command = new AddCommand();

        $form=$this->createForm(AddType::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                $this->addFlash('success', 'Nouveau post créé !');

                return $this->redirectToRoute('admin_posts_index');
            } catch (Exception $e) {
                $this->addFlash('error', sprintf('Error: %s', $e->getMessage()));
            }
        }

        return $this->render('admin/posts/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
