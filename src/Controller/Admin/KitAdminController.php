<?php

namespace App\Controller\Admin;

use App\Command\Command\Admin\Kit\AddCommand;
use App\Command\CommandHandler\Admin\Kit\AddCommandHandler;
use App\Entity\Kit;
use App\Form\Admin\Kit\AddType;
use App\Repository\KitRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class KitAdminController extends AbstractController
{
    /**
     * @Route("/admin/kit" , name="admin_kit_index")
     * @param KitRepository $kitRepository
     * @return Response
     */
    public function index(KitRepository $kitRepository): Response
    {
        return $this->render('admin/kit/index.html.twig', [
            'kits' => $kitRepository->findAll()
        ]);
    }

    /**
     * @Route("/admin/kit/add" , name="admin_kit_add")
     * @param KitRepository $kitRepository
     * @return Response
     */
    public function add(Request $request, AddCommandHandler $handler): Response
    {
        $command = new AddCommand();

        $form=$this->createForm(AddType::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                $this->addFlash('success', 'Nouveau coutenu pour le kit créé !');

                return $this->redirectToRoute('admin_kit_index');
            } catch (Exception $e) {
                $this->addFlash('error', sprintf('Error: %s', $e->getMessage()));
            }
        }

        return $this->render('admin/kit/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/kit/edit/{id}" , name="admin_kit_edit")
     * @param KitRepository $kitRepository
     * @param Kit $kit
     * @return Response
     */
    public function edit(Request $request,Kit $kit, AddCommandHandler $handler): Response
    {
        $command = new AddCommand();
        $command->id = $kit->getId();
        $command->contenu = $kit->getContenu();
        $command->date = $kit->getDate();

        $form=$this->createForm(AddType::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                $this->addFlash('success', 'Nouveau coutenu du kit édité !');

                return $this->redirectToRoute('admin_kit_index');
            } catch (Exception $e) {
                $this->addFlash('error', sprintf('Error: %s', $e->getMessage()));
            }
        }

        return $this->render('admin/kit/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
