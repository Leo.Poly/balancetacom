<?php

namespace App\Controller;

use App\Command\Command\Profil\ProfilCommand;
use App\Command\CommandHandler\Profil\ProfilCommandHandler;
use App\Entity\User;
use App\Form\Profil\DisplayType;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProfilController extends AbstractController
{

    /**
     * @Route("/profil/{id}" , name="app_profil")
     * @return Response
     */
    public function display(Request $request,User $user, ProfilCommandHandler $handler): Response
    {
        $command = new ProfilCommand();
        $command->id = $user->getId();
        $command->profession = $user->getProfession();
        $command->dateNaissance = $user->getDateNaissance();
        $command->prenom = $user->getPrenom();
        $command->nom = $user->getNom();
        $command->tel = $user->getTel();
        $command->email = $user->getEmail();

        $form=$this->createForm(DisplayType::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                $this->addFlash('success', 'Profil mis à jour !');

                return $this->redirectToRoute('main');
            } catch (Exception $e) {
                $this->addFlash('error', sprintf('Error: %s', $e->getMessage()));
            }
        }

        return $this->render('profil/display.html.twig', [
            'user' =>  $this->getUser(),
            'form' => $form->createView(),
        ]);
    }
}
