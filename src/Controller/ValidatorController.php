<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ValidatorController extends AbstractController
{
    #[Route('/validator', name: 'app_validator')]
    public function index(): Response
    {
        return $this->render('validator/index.html.twig', [
            'controller_name' => 'ValidatorController',
        ]);
    }
}
