<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class KitController extends AbstractController
{
    #[Route('/kit', name: 'app_kit')]
    public function index(): Response
    {
        return $this->render('kit/index.html.twig', [
        ]);
    }

    /**
     * @Route("/page/1" , name="app_kit_page1")
     * @return Response
     */
    public function page1(): Response
    {
        return $this->render('kit/partie1.html.twig', [

        ]);
    }
    /**
     * @Route("/page/2" , name="app_kit_page2")
     * @return Response
     */
    public function page2(): Response
    {
        return $this->render('kit/partie2.html.twig', [

        ]);
    }

    /**
     * @Route("/page/3" , name="app_kit_page3")
     * @return Response
     */
    public function page3(): Response
    {
        return $this->render('kit/partie3.html.twig', [

        ]);
    }

    /**
     * @Route("/ressources" , name="app_kit_ressources")
     * @return Response
     */
    public function ressources(): Response
    {
        return $this->render('kit/ressources.html.twig', [

        ]);
    }
}
