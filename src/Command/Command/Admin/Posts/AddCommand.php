<?php

namespace App\Command\Command\Admin\Posts;

use DateTime;

class AddCommand
{
    /** @var int */
    public $id;

    public $description;

    public $redacteur;

    public $datePublication;

    public $image;

}