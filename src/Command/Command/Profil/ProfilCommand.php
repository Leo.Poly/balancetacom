<?php

namespace App\Command\Command\Profil;

use DateTime;

class ProfilCommand
{
    /** @var int */
    public $id;

    public $nom;

    public $prenom;

    public $dateNaissance;

    public $email;

    public $password;

    public $verifyPassword;

    public $tel;

    public $profession;

}