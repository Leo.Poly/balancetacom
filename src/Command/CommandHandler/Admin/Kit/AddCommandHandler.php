<?php

namespace App\Command\CommandHandler\Admin\Kit;

use App\Command\Command\Admin\Kit\AddCommand;
use App\Entity\Kit;
use App\Repository\KitRepository;
use DateTime;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class AddCommandHandler
{

    /**
     * @param KitRepository $kitRepository
     */
    public function __construct
    (
         KitRepository $kitRepository,

    ) {
        $this->kitRepository = $kitRepository;
    }

    /**
     * @param AddCommand $command
     * @throws \Exception
     */
    public function handle(AddCommand $command)
    {

        $id = $command->id;

        if (empty($id)) {
            $kit = new Kit();
        } else {
            /** @var Kit $qualityControl */
            $kit = $this->kitRepository->get($id);
            if (!$kit) {
                throw new \Exception(sprintf('Error'));
            }
        }

        $kit->setContenu($command->contenu);
        $kit->setCategorie($command->categorie);
        $kit->setDate(new \DateTime());

        $this->kitRepository->save($kit);
    }
}