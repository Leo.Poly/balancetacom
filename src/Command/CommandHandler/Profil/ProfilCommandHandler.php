<?php

namespace App\Command\CommandHandler\Profil;


use App\Command\Command\Profil\ProfilCommand;
use App\Entity\Kit;
use App\Entity\User;
use App\Repository\UserRepository;
use DateTime;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;

class ProfilCommandHandler
{

    /**
     * @param UserRepository $userRepository
     */
    public function __construct
    (
         UserRepository $userRepository,
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * @param ProfilCommand $command
     * @throws \Exception
     */
    public function handle(ProfilCommand $command)
    {

        $id = $command->id;
        /** @var User $user */
        $user = $this->userRepository->getById($id);
        if (!$user) {
            throw new \Exception(sprintf('Error'));
        }

        $user->setNom($command->nom);
        $user->setPrenom($command->prenom);
        $user->setDateNaissance($command->dateNaissance);
        $user->setEmail($command->email);
        $user->setTel($command->tel);
        $user->setProfession($command->profession);
        $user->setEmail($command->email);

        $this->userRepository->save($user);
    }
}